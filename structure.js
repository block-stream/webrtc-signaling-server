const { getDistanceBetweenIps } = require("./ipLocate");

class Structure {
  constructor() {
    // peer lookup
    this.peers = {};
    this.cdnPeers = {};

    // resource peer lookup
    this.resource = {};

    // sockets lookup
    this.sockets = {};
  }

  getPeerFromSocket(socket) {
    if (socket.id in this.sockets) return this.sockets[socket.id].username;
    return undefined;
  }

  getSocketFromPeer(username) {
    const peer = this.peers[username] || this.cdnPeers[username];  
    return this.sockets[peer.socket].socket;
  }

  registerCdnPeer(cid, cdnSocket) {
    this.cdnPeers[cid] = { username: cid, socket: cdnSocket.id };
    this.sockets[cdnSocket.id] = { socket: cdnSocket, username: cid };
  }

  // limitation: assuming one peer only leeching one resource
  registerNewLeecher(user, resource, userSocket) {
    this.peers[user] = {
      username: user,
      resource: resource,
      socket: userSocket.id
    };

    this.sockets[userSocket.id] = { socket: userSocket, username: user};

    this.registerResourcePeer(user, resource);
  }

  removeLeecher(user) {
    const resource = this.peers[user].resource;
    this.removeResourcePeer(user, resource);
  }

  registerResourcePeer(user, resource) {
    if (this.resource[resource] === undefined)
      this.resource[resource] = new Set();

    this.resource[resource].add(user);
  }

  removeResourcePeer(user, resource) {
    this.resource[resource].delete(user);
    delete this.sockets[this.peers[use].socket];
    delete this.peers[user];
  }

  updatePeerContents(user, loaded) {
    const peer = this.peers[user] || this.cdnPeers[user];
    peer.loaded = loaded;
  }

  // whether rangeA is in rangeB
  isRangeInRange(rangeA, rangeB) {
    console.log('rir', rangeA, rangeB)
    return rangeA.from >= rangeB.from && rangeA.to <= rangeB.to;
  }

  // returns usernames
  searchPeersForResource(resource, neededByteRange) {
    let array = [];
    if (resource in this.resource)
      array = array.concat(Array.from(this.resource[resource]));

    array = array.concat(Object.keys(this.cdnPeers));

    console.log(array);

    return array
      .map((username) => this.peers[username] || this.cdnPeers[username])
      .filter((peer) => this.isRangeInRange(neededByteRange, peer.loaded))
      .map((peer) => peer.username);
  }

  getPeerIp(username) {
    const socket = this.getSocketFromPeer(username);
    //console.log(socket, this.sockets)
    return (
      socket.handshake.headers["x-forwarded-for"] ||
      socket.conn.remoteAddress.split(":")[3]
    );
  }

  // args: usernames
  async rankPeersByExpectedPerformance(leecher, seeders) {
    return seeders;
    const leecherIp = this.getPeerIp(leecher);

    const distances = await Promise.all(
      seeders.map((seeder) => {
        const seederIp = this.getPeerIp(seeder);
        return getDistanceBetweenIps(leecherIp, seederIp);
      })
    );

    const seedersCopy = seeders.map((seeder, index) => ({ seeder, index }));
    seedersCopy.sort(
      (seeder1, seeder2) => distances[seeder2.index] - distances[seeder1.index]
    );

    return seedersCopy.map(({ seeder }) => seeder);
  }
}

module.exports = { Structure };
