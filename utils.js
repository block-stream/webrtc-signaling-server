const crypto = require('crypto');

const computeHash = (passwd) => {
  const hash = crypto.createHash('md5');
  hash.update(passwd);
  return hash.digest('hex');
};

module.exports = { computeHash };
