const app = require("express")();
const server = require("http").Server(app);
const io = require("socket.io")(server);
const { Structure } = require("./structure");

server.listen(8000, () => console.log("listening at 8000"));
// WARNING: app.listen(80) will NOT work here!

const STRUCTURE = new Structure();

io.on("connection", (socket) => {
  console.log("socket connection established");

  // register cdn peer
  socket.on("cdn-peer", (data) => {
    const { cid } = data;
    console.log(`a cdn peer just joined: ${cid}`)
    STRUCTURE.registerCdnPeer(cid, socket);
    STRUCTURE.updatePeerContents(cid, { from: -Infinity, to: Infinity})
  });

  // register leecher
  socket.on("movie/start", (data) => {
    const { movie_id, username } = data;

    if (username === undefined) {
      console.log(
        `unauthorized user tried to access the content, terminating connection.`
      );
      return socket.disconnect();
    }

    console.log(`user ${username} wants to stream the movie`);
    STRUCTURE.registerNewLeecher(username, movie_id, socket);
  });

  // loaded state
  socket.on('movie/loaded-status', ({ from, to, username, movie_id}) => {
    console.log(`updating buffer state of peer ${username}`, from, to, movie_id)
    STRUCTURE.registerResourcePeer(username, movie_id)
    STRUCTURE.updatePeerContents(username, {from, to});
  });

  // exchange ice candidates
  socket.on('ice', ({ username, peer, candidate }) => {
    console.log(`passing ice from ${username} to ${peer}`)
    const peerSocket = STRUCTURE.getSocketFromPeer(peer);
    peerSocket.emit('ice', { peer: username, candidate: candidate });
  });

  // requesting content
  socket.on('movie/fragment', (data) => {
    let { start, end, movie_id, username } = data;
    console.log(`${username} is requesting ${start}-${end} bytes of ${movie_id}`);    
    
    if (start === undefined)
      start = -Infinity;
    if (end === undefined)
      end = Infinity;

    const peers = STRUCTURE.searchPeersForResource(movie_id, { from: start, to: end });
    console.log(`peers for fragment ${movie_id}:${start}-${end}`, peers)
    STRUCTURE.rankPeersByExpectedPerformance(username, peers)
      .then(seeders => seeders.map(seeder => socket.emit('generate-ice', { seeder })));
  });

  // remove peer on socket disconnect
  socket.on("disconnect", (socket) => {
    // cleanup after that peer;
    const peer = STRUCTURE.getPeerFromSocket(socket);
    if (peer) {
      console.log(`peer disconneted: ${peer}`)
      STRUCTURE.removeLeecher(peer);
    }
  });
});
