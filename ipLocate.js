const fetch = require("node-fetch");

const deg2rad = deg => {
  return deg * (Math.PI / 180);
};

const getDistanceFromLatLonInKm = (cord1, cord2) => {
  console.log(cord1, cord2)
  const lat1 = cord1.lat;
  const lon1 = cord1.lon;
  const lat2 = cord2.lat;
  const lon2 = cord2.lon;

  const R = 6371; // Radius of the earth in km
  const dLat = deg2rad(lat2 - lat1); // deg2rad below
  const dLon = deg2rad(lon2 - lon1);
  const a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) *
      Math.cos(deg2rad(lat2)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  const d = R * c; // Distance in km
  return d;
};

// docs: https://ip-api.com/docs/api:json
const getIpLatLong = ip => {
  return fetch(`http://ip-api.com/json/${ip}`)
    .then(res => res.json())
    .then(data => { console.log(ip, data); return ({ lat: data.lat, lon: data.lon }) });
};

const getDistanceBetweenIps = (ip1, ip2) => {
  return Promise.all([
    getIpLatLong(ip1),
    getIpLatLong(ip2)
  ]).then((cord1, cord2) => getDistanceFromLatLonInKm(cord1, cord2));
};

module.exports = { getDistanceBetweenIps };
