const io = require('socket.io-client');
const db = require('./db');
const SimplePeer = require('simple-peer');
const wrtc = require('wrtc');

// connect to the signaling server as cdn peer
const socket = io(process.env.SINGALING_SERVER || 'http://localhost:8000');

// let the server know you are a cdn peer
socket.emit('cdn-peer', { cid: 'sole'});

const peers = {

};

socket.on('ice', ({ peer, candidate }) => {
  const thisPeer = peers[peer] || new SimplePeer({ wrtc });
  peers[peer] = thisPeer;

  thisPeer.on('signal', data => {
    socket.emit('ice', { peer: peer, candidate: data, username: 'sole' });
  });

  thisPeer.signal(candidate);

  thisPeer.on('connect', () => thisPeer.send('here is the movie fragment you need...'));

  const clearPeer = () => delete peers[peer];
  thisPeer.on('error', clearPeer)
  thisPeer.on('close', clearPeer)
});
