const mongoose = require("mongoose");
const { computeHash } = require("./utils");

const connection = mongoose.connect(
  process.env.MONGO_URL || "mongodb://localhost:/test",
  {
    useNewUrlParser: true,
    dbName: "blockstream"
    //user: process.env.MONGO_USER,
    //pass: process.env.MONGO_PASS
  }
);

const userSchema = new mongoose.Schema({
  first_name: {
    type: String,
    required: true
  },
  last_name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unque: true
  },
  password: {
    type: String,
    required: true
  }
});

userSchema.methods.verifyPassword = function (passwd) {
  console.log(
    computeHash(passwd),
    new Buffer(this.password, "binary").toString()
  );
  return computeHash(passwd) === this.password;
};

const contentSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String
  },
  price: {
    type: Number,
    required: true
  },
  // email of user who uploaded the content
  content_owner: {
    type: String,
    required: true
  },
  upload_timestamp: {
    type: Date,
    default: Date.now
  },
  language: {
    type: String,
    required: true,
  },
  content_id: {
    type: String,
    required: true,
  }
});

const purchaseSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true
  },
  content_id: {
    type: mongoose.ObjectId,
    required: true
  }
});

const movieSchema = {
  movie_url: String,
  poster: String,
  name: String,
  directors: [String],
  actors: [String],
  productions: [String],
  description: String,
  movie_id: String,
  movie_contents: Buffer,
  mimetype: String
};

const indianMovieSchema = new mongoose.Schema(movieSchema, {
  collection: "movies.indian"
});
const featuredMovieSchema = new mongoose.Schema(movieSchema, {
  collection: "movies.featured"
});
const westernMovieSchema = new mongoose.Schema(movieSchema, {
  collection: "movies.western"
});

const models = {
  User: mongoose.model("User", userSchema),
  Content: mongoose.model("Content", contentSchema),
  Purchase: mongoose.model("Purchase", purchaseSchema),
  IndianMovie: mongoose.model("IndianMovie", indianMovieSchema),
  FeaturedMovie: mongoose.model("FeaturedMovie", featuredMovieSchema),
  WesternMovie: mongoose.model("WesternMovie", westernMovieSchema)
};

module.exports = { models };
